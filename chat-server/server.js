const express = require('express');
const cors = require('cors');
const app = express();
const expressWs = require('express-ws')(app);

const port = 8000;

app.use(cors());

const clients = {};
let pixelsArray = [];

app.ws('/chat', (ws, req) => {
    const id = req.get('sec-websocket-key');
    clients[id] = ws;

    console.log('Client connected');
    console.log('Number of active connections: ', Object.values(clients).length);

    let username;

    ws.send(JSON.stringify({
        type: 'NEW_IMAGE',
        pixels: pixelsArray,
    }));

    ws.on('message', (msg) => {
        let decodedMessage;
        try {

            decodedMessage = JSON.parse(msg);
            pixelsArray = pixelsArray.concat(decodedMessage.pixels);
        } catch (e) {
            return ws.send(JSON.stringify({
                type: 'ERROR',
                message: 'Message is not JSON'
            }));
        }

        switch (decodedMessage.type) {
            case 'SET_USERNAME':
                username = decodedMessage.username;
                break;
            case 'CREATE_IMAGE':
                Object.values(clients).forEach(client => {
                    client.send(JSON.stringify({
                        type: 'NEW_IMAGE',
                        pixels: pixelsArray,
                    }));
                });
                break;
            default:
                return ws.send(JSON.stringify({
                    type: 'ERROR',
                    message: 'Unknown message type'
                }));
        }
    });

    ws.on('close', (msg) => {
        delete clients[id];
        console.log('client disconnected');
        console.log('Number of active connections', Object.values(clients).length);
      delete clients[id];
    });

});

app.listen(port, () => {
    console.log(`server started on ${port} `);
});