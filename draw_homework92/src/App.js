import React, { Component } from 'react';

class App extends Component {
    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/chat');

        this.websocket.onmessage = (message) => {
            const decodedMessage = JSON.parse(message.data);

            switch (decodedMessage.type) {
                case 'NEW_IMAGE':
                    this.context = this.canvas.getContext('2d');
                    this.imageData = this.context.createImageData(1, 1);
                    this.d = this.imageData.data;

                    this.d[0] = 0;
                    this.d[1] = 0;
                    this.d[2] = 0;
                    this.d[3] = 255;
                    decodedMessage.pixels.forEach((pixel) => {
                        this.context.putImageData(this.imageData, pixel.x, pixel.y);
                    });

                    break;
            }
        };
    };

    state = {
        mouseDown: false,
        pixelsArray: []
    };

    canvasMouseMoveHandler = event => {
      if (this.state.mouseDown) {
          event.persist();
          this.setState(prevState => {
              return {
                  pixelsArray: [...prevState.pixelsArray, {
                      x: event.clientX,
                      y: event.clientY
                  }]
              };
          });
          this.context = this.canvas.getContext('2d');
          this.imageData = this.context.createImageData(1, 1);
          this.d = this.imageData.data;

          this.d[0] = 0;
          this.d[1] = 0;
          this.d[2] = 0;
          this.d[3] = 255;

          this.context.putImageData(this.imageData, event.clientX, event.clientY);
        }
    };

    mouseDownHandler = event => {
       this.setState({mouseDown: true});
    };

    mouseUpHandler = event => {
        this.websocket.send(JSON.stringify({
            type: 'CREATE_IMAGE',
            pixels: this.state.pixelsArray
        }));

        this.setState({mouseDown: false, pixelsArray: [] });
    };

    render() {
        return (
          <div>
              <canvas
                  ref={elem => this.canvas = elem}
                  style = {{border: '1px solid black'}}
                  width={800}
                  height={600}
                  onMouseDown={this.mouseDownHandler}
                  onMouseUp={this.mouseUpHandler}
                  onMouseMove={this.canvasMouseMoveHandler}
              />
          </div>
        );
    }
}

export default App;
